use regex::Regex;
use robot_core::robot_core_client::RobotCoreClient;
use structopt::clap::AppSettings;
use structopt::StructOpt;
use tonic::transport::Channel;

pub mod base {
    tonic::include_proto!("base");
}

pub mod robot_core {
    tonic::include_proto!("robot_core");
}

pub mod google {
    pub mod protobuf {
        tonic::include_proto!("google.protobuf");
    }
}

/// yyservice is a command-line tool for printing information about services
#[derive(StructOpt, Debug)]
#[structopt(setting = AppSettings::ColoredHelp)]
#[structopt(setting = AppSettings::DisableVersion)]
#[structopt(verbatim_doc_comment)]
enum Opt {
    /// list active services
    #[structopt(setting = AppSettings::ColoredHelp)]
    #[structopt(setting = AppSettings::DisableVersion)]
    #[structopt(setting = AppSettings::ArgRequiredElseHelp)]
    List {
        /// list all services
        #[structopt(short, long)]
        all: bool,
    },

    /// call the service with provided args
    #[structopt(setting = AppSettings::ColoredHelp)]
    #[structopt(setting = AppSettings::DisableVersion)]
    #[structopt(setting = AppSettings::ArgRequiredElseHelp)]
    Call {
        /// service method ('/node/method')
        method: String,
        /// method request args (json format)
        request: String,
    },
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let core_address = std::env::var("YY_CORE_ENDPOINT").unwrap_or("[::1]:11311".to_string());
    let core_address = format!("http://{}", core_address);
    println!("connecting to robot core: {}", core_address);

    if let Ok(mut cli) = RobotCoreClient::connect(core_address.clone()).await {
        match opt {
            Opt::List { all } => svc_list(&mut cli, all).await?,
            Opt::Call { method, request } => svc_call(&mut cli, method, request).await?,
        }
    } else {
        println!("Connect to robot_core at {} failed !!!", core_address);
        println!("You should check robot_core if it has started.");
        println!(
            "Or if robot_core has already started at remote host, you can set YY_CORE_ENDPOINT:"
        );
        println!("\t`export YY_CORE_ENDPOINT=<ip:port>'");
    }

    Ok(())
}

async fn svc_list(
    cli: &mut RobotCoreClient<Channel>,
    _all: bool,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut req = tonic::Request::new(google::protobuf::Empty {});
    req.metadata_mut()
        .insert("__istools", "true".parse().unwrap());
    req.set_timeout(std::time::Duration::from_secs(3));

    let reply = cli.list_method(req).await?.into_inner();
    println!("all services:");
    for v in reply.methods.iter() {
        println!("  - /{}/{}", v.service, v.method);
    }

    Ok(())
}

async fn svc_call(
    cli: &mut RobotCoreClient<Channel>,
    method: String,
    request: String,
) -> Result<(), Box<dyn std::error::Error>> {
    // method -- (/<a>/<b>)
    let re = Regex::new(r"^/(\w+)/(\w+)$").unwrap();
    if let Some(caps) = re.captures(&method) {
        let s = caps.get(1).unwrap().as_str();
        let m = caps.get(2).unwrap().as_str();
        let mut req = tonic::Request::new(base::InvokeRequest {
            service: s.to_string(),
            method: m.to_string(),
            msg: request,
        });
        req.set_timeout(std::time::Duration::from_secs(3));

        let reply = cli.invoke(req).await?.into_inner();
        println!("call result:\n  code: {}", reply.code);
        println!("  msg: {}", reply.msg);
        println!("  reply: {}", reply.data);
    } else {
        println!("error method: {}", method);
    }

    Ok(())
}
